import Client from './api/bundle.js';

const apiClient = new Client();

apiClient.connect();

var chat = {
    user : {
        name : "Remco",
        avatar : "./images/avatar.png"
    },
    channels : [
        {
            name : 'Daily'
        },
        {
            name : 'General'
        }
    ],
    currentChannel : 'Daily',
    messages : [],
    searching : false,
    searchText : ''
}

Vue.component('channel-list', {
    data() {
        return chat;
    },

    template : `
        <div class="channel-list">
            <h2 class="channel-list__header">Channels</h2>
            <ul>
                <li v-for="channel of channels" class="channel-list__item" v-bind:class="{'channel-list__item--current' : channel.name === currentChannel}">
                    <span class="channel-list__bullet">#</span> {{ channel.name }}
                </li>
            </ul>
        </div>
    `
});

Vue.component('app-header', {
    template : `
        <header class="app-header">
            <h2 class="app-header__title">#_general</h2>
            <search-form></search-form>
        </header>
    `
})

Vue.component('search-form', {
    template : `
        <form @submit.prevent="onSearch" class="search-form">
            <input type="search" v-model="searchText" placeholder="Search" class="reset-text search-field">
        </form>
    `,
    data() {
        return chat;
    },
    methods : {
        onSearch() {
            this.searching = true;
        }
    }
})

Vue.component('search-sidebar', {
    template : `
        <transition name="slide">
            <aside class="search-sidebar" v-show="searching">
                <button v-on:click="stopSearching" class="reset-button search-sidebar__close" aria-label="Close" title="Close"></button>
                <h2 class="search-sidebar__header">{{ filteredMessages.length }} Messages</h2>
                <message-list v-bind:messages="filteredMessages">
                    <template slot="empty">
                        No messages found for "{{ searchText }}"
                    </template>
                </message-list>
            </aside>
        </transition>
    `,

    data() {
        return chat;
    },

    methods : {
        stopSearching() {
            this.searching = false;
        }
    },

    computed : {
        filteredMessages() {
            return this.messages.filter(message => message.data.indexOf(this.searchText) > -1);
        }
    }
})

Vue.component('app-sidebar', {
    template : `
        <aside class="app-sidebar">
            <h1 class="app-sidebar__title">
                De Voorhoede
            </h1>
            <user-profile></user-profile>
            <channel-list></channel-list>
        </aside>
    `
});

Vue.component('user-profile', {
    data() {
        return chat; //chat becomes reactive when you return it as data (show what happens)
    },

    template : `
        <div class="user-profile">
            <label class="user-profile__label" for="name">Name</label>
            <input type="text" id="name" class="reset-text user-profile__name" v-model="user.name">
        </div>
    `
});

Vue.component('chat-section', {
    data() {
        return chat; //chat becomes reactive when you return it as data (show what happens)
    },

    //- factor out in individual components
    //- make the chat object at the top available as data in the chat-area
    //- pass the messages as a property
    //- show what happens when you assign non reactive data
    //- show the username with the message (without changing the api call)
    template : `<section class="chat-section">
        <div class="chat-section__messages">
            <message-list v-bind:messages="messages"></message-list>
        </div>
        <message-field class="chat-section__field" v-on:submit="addMessage"></message-field>
    </section>`,

    methods : {
        addMessage(text) {
            let message = {
                user : this.user,
                id : this.messages.length+1,
                date : new Date()
            }

            if(text === '/cat') {
                message.type = 'cat';
                message.data = 'http://thecatapi.com/api/images/get?format=src&type=gif'
            }
            else {
                message.type = 'text';
                message.data = text;
            }

            this.messages.push(message);
            this.searchText = '';
        }
    }
});

//we want to create multiple type of messages
//Create a class based component for TextMessage and GifMessage
//create a generic message holder
//use slots to define areas for the "content" and "header" of the message
//advanced: expose dateFormatted in slot-scope
Vue.component('message', {
    props : {
        user : {type : Object},
        date : {type : Date}
    },

    template : `
        <li class="message">
            <div class="message__header">
                <slot name="header">
                    <img class="message__avatar" v-bind:src="user.avatar">
                    <strong>{{user.name}}</strong> 
                    <span class="message__date">{{dateFormatted}}</span>
                </slot>
            </div>
            <div class="message__content">
                <slot name="content"></slot>
            </div>
        </li>
    `,

    computed : {
        dateFormatted() {
            return new Date(this.date).toLocaleTimeString();
        }
    }
});

//factor out message into a separate component
Vue.component('text-message', {
    props : {
        message : {
            type : Object, 
            validator(v) {
                return 'user' in v && 'data' in v && 'date' in v;;
            }
        }
    },

    //create a directive to show a tooltip
    template : `
        <message v-bind:user="message.user" v-bind:date="message.date">
            <template slot="content">
                {{message.data}}
            </template>
        </message>
    `
});

Vue.component('cat-message', {
    props : {
        message : {
            type : Object, 
            validator(v) {
                return 
                    'user' in v && 
                    'data' in v &&
                    'date' in v;
            }
        }
    },

    template : `
        <message v-bind:user="message.user" v-bind:date="message.date">
            <template slot="content">
                <img :src="message.data" />
            </template>
        </message>
    `
});

Vue.component('message-list', {
    props : {
        messages : {type : Array}
    },

    //add a text to show when no messages are available
    //try v-show and v-if and "notice the difference"
    template : `
        <div class="message-list">
            <div v-if="messages.length === 0" class="message-list__empty">
                <slot name="empty">No messages</slot>
            </div>
            
            <transition-group name="fade" tag="ol" v-else class="message-list__list">
                <template v-for="message of messages">
                    <text-message v-if="message.type === 'text'" v-bind:key="message.id" v-bind:message="message"></text-message>
                    <cat-message v-else-if="message.type === 'cat'" v-bind:key="message.id" v-bind:message="message"></cat-message>
                </template>
            </transition-group>
        </div>
    `
});

Vue.component('message-field', {
    template : `<form class="message-field" v-on:submit.prevent="submit">
        <input class="reset-text message-field__input" type="text" v-model="value" autofocus>
        <input class="message-field__submit" type="submit" :disabled="empty">
    </form>`,

    data() {
        return {
            value : ''
        }
    },

    methods : {
        submit() {
            this.$emit('submit', this.value);
            this.value = '';
        }
    },

    computed : {
        //only allow submit of message when message is not empty
        empty() {
            return this.value.trim().length === 0;
        }
    }
});

const app = new Vue({
    el: '.app',

    data() {
        return chat;
    },

    template : `
        <div class="app" :class="{'app--searching' : searching}">
            <app-header></app-header>
            <app-sidebar></app-sidebar>
            <chat-section></chat-section>
            <search-sidebar></search-sidebar>
        </div>
    `
});

Vue.config.devtools = true