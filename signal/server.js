const WebSocket = require('ws');

const wss = new WebSocket.Server({port : 9999});

wss.on('connection', ws => {
    ws.on('message', message => {
        wss.clients.forEach(client => {
            if(client !== ws && client.readyState === WebSocket.OPEN) {
                console.log('send to other client')
                client.send(message);
            }
        })
    });

    ws.on('error', error => {
        console.log(error);
    });
});