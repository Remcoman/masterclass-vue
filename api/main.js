/*
    - create connection through webrtc
    - get messages from indexeddb
    - onpeer
        - request messages newer then x
    - complete chain

*/ 

import Rx from 'rxjs/Rx';

// function signal(offer) {
//     const remoteCon = new RTCPeerConnection({
//         iceServers : [
//             {urls : ['stun:stun.l.google.com:19302', 'stun:iphone-stun.strato-iphone.de:3478']}
//         ]
//     });

//     return remoteCon.setRemoteDescription(new RTCSessionDescription(offer))
//         .then(() => {
//             return remoteCon.createAnswer();
//         })
//         .then(answer => {
//             return remoteCon.setLocalDescription(answer)
//                 .then(() => {
//                     return answer;
//                 });
//         });
// }

function createSignalServer() {
    return Rx.Observable.webSocket({
        url : 'ws://localhost:9999'
    });
}

class Connection {
    constructor() {

    }
}

export default class {
    connect() {
        const signal$ = createSignalServer();

        this.con = new RTCPeerConnection({
            iceServers : [
                {urls : ['stun:stun.l.google.com:19302', 'stun:iphone-stun.strato-iphone.de:3478']}
            ]
        });
            
        this.gotIceCandidate$ = Rx.Observable.fromEvent(this.con, 'icecandidate')
            .filter(e => !!e.candidate)
            .map(e => e.candidate)
            .share()

        this.gotLocalOffer$ = Rx.Observable.fromEvent(this.con, 'signalingstatechange')
            .filter(({target}) => !!target.localDescription.sdp)
            .share()

        this.gotRemoteOffer$ = Rx.Observable.fromEvent(this.con, 'signalingstatechange')
            .filter(({target}) => !!target.remoteDescription.sdp)
            .share()

        Rx.Observable.fromEvent(this.con, 'negotiationneeded')
            .map(e => {
                this.con.createOffer()
                    .then(offer => {
                        console.log(offer, this.con);
                        return this.con.setLocalDescription(offer)
                            .then(() => {
                                console.log('sending offer');
                                signal$.next( JSON.stringify(offer.toJSON()) );
                            });
                    });
            })
            .subscribe();

        //add ice candidates
        this.gotIceCandidate$
            .do(() => console.log('got ice candidate'))
            .buffer( this.gotRemoteOffer$ ) //buffer until we have remote offer
            .concatMap(candidates => {
                return Rx.Observable.of( ...candidates.map(candidate => {
                    return Rx.Observable.from( this.con.addIceCandidate(new RTCIceCandidate(candidate)) );
                }) );
            })
            .do(() => console.log('ice ice baby'))
            .subscribe();

        signal$
            .filter( data => data.type === 'offer' )
            .do(offer => console.log('got offer', offer))
            .mergeMap(data => {
                return Rx.Observable.from(  
                    this.con.setRemoteDescription(data)
                        .then(() => this.con.createAnswer())
                        .then(answer => signal$.next( JSON.stringify(answer.toJSON()) ))
                );
            })
            .subscribe();

        signal$
            .filter( data => data.type === 'answer' )
            .do(answer => console.log('got answer', answer))
            .skipWhile(() => this.con.remoteDescription.sdp)
            .mergeMap(data => {
                console.log(this.con.remoteDescription);
                return Rx.Observable.from(  
                    this.con.setRemoteDescription(data)
                );
            })
            .subscribe();

        const ch = this.con.createDataChannel('messages');
    }

    addMessage(type, data, userID) {
        
    }
}