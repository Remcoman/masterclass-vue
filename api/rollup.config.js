import livereload from 'rollup-plugin-livereload'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'main.js',
  output : {
    name : 'api',
    format : "es",
    file : "bundle.js",
    sourcemap: true
  },
  
  plugins: [
    livereload(),
    resolve({
        jsnext: true,  // Default: false
        main: true,  // Default: true
        modules : true
    }),
    commonjs(),
  ]
}